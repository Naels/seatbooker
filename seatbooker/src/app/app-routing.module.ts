import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router'
import { FilmspanelComponent } from './components/filmspanel/filmspanel.component';
import { FilminfosComponent } from './components/filminfos/filminfos/filminfos.component';
import { MainComponent } from './components/main/main/main.component';
import { MoviereservationComponent } from './components/moviereservation/moviereservation.component';

const routes: Routes = [
  { path: '',                                 component:MainComponent},
  { path: 'film/:id',                         component:FilminfosComponent},
  { path: 'reservation/:idShowtime/:idMovie', component:MoviereservationComponent}
]



@NgModule({
  declarations: [],
  imports: [CommonModule,
  RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
