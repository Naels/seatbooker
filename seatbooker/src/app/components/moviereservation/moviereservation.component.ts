import { Component, OnInit } from '@angular/core';
import { SeatService } from 'src/app/services/seat/seat.service';
import { ActivatedRoute, Router} from '@angular/router'
import { ShowtimeService } from 'src/app/services/showtime/showtime.service';

@Component({
  selector: 'app-moviereservation',
  templateUrl: './moviereservation.component.html',
  styleUrls: ['./moviereservation.component.css']
})
export class MoviereservationComponent implements OnInit {

  allSeats                  : any
  availableSeats            : any
  getParamId                : any
  movieID                   : any
  showtimeDay               : any
  showtimeMonth             : any
  hourShowtime              : any
  selectedSeatsTabToUpdate  : any[] = []
  selectedSeatsTotal        = 0

  months: any[] = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];

  constructor(private seatService     : SeatService,
              private showtimeService : ShowtimeService,
              private route           : ActivatedRoute,
              private router          : Router ) { }


  ngOnInit(): void {
    this.getParamId = this.route.snapshot.paramMap.get('idShowtime')
    this.movieID    = this.route.snapshot.paramMap.get('idMovie')
    this.getSeatByShowtimeId(this.getParamId)
    this.getCountAvailableSeatsByShowtime(this.getParamId)
    this.getShowtimeInfos(this.getParamId)
  }

  getSeatByShowtimeId(showtimeId: any) {
    this.seatService.getSeatByShowtimeId(showtimeId).subscribe((res) => {
      this.allSeats = res.seats
      this.buildSeatContainer(res.seats)
    }, (err) => console.log(err))
  }

  getCountAvailableSeatsByShowtime(showtimeId: any) {
    this.seatService.getCountAvailableSeatsByShowtime(showtimeId).subscribe((res) => {
      this.availableSeats = res.availableSeats
    }, (err) => console.log(err))
  }

  getShowtimeInfos(showtime_id: any) {
    this.showtimeService.getShowtimeId(showtime_id).subscribe((res) => {
      this.showtimeDay = new Date(res[0].show_date).getDate() - 1
      this.showtimeMonth = this.months[new Date(res[0].show_date).getMonth()] 
      this.hourShowtime = res[0].start_time.slice(0,5)
    }, (err) => console.log(err))
  }


  buildSeatContainer(seatsList: any) {
    let seatContainer = document.getElementById("seatContainer")
    let table = document.createElement('table');
    table.className = 'custom-table'

    for (let rowIndex = 0; rowIndex < seatsList.length / 6; rowIndex++) {
      let row = document.createElement('tr')
      let startIdx = rowIndex * 6;
      let endIdx = startIdx + 6;
      let rowSeats = seatsList.slice(startIdx, endIdx);
      // let shiftAmount = rowIndex % 2 === 0 ? '50%' : '-50%';
      for (let seatData of rowSeats) {
        let cell = document.createElement('td')
        cell.className = seatData.is_taken ? 'taken' : 'available'
        cell.addEventListener('click', () => this.handleSeatClick(seatData, cell));
        let seatIcon = document.createElement('span');
        seatIcon.className = 'material-icons';
        seatIcon.textContent = 'event_seat';
        // seatIcon.style.marginLeft = shiftAmount;
        cell?.appendChild(seatIcon)
        row?.appendChild(cell)
      }
      table?.appendChild(row)
    }
    seatContainer?.appendChild(table)
  }

  handleSeatClick(seatData: any, cell: any) {
    if (!seatData.is_taken) {
      const seatIndex = this.selectedSeatsTabToUpdate.indexOf(seatData.seat_id);
      if (seatIndex === -1) {
        this.selectedSeatsTabToUpdate.push(seatData.seat_id);
        cell.classList.add('selected');
        this.selectedSeatsTotal += 1;
      } else {
        this.selectedSeatsTabToUpdate.splice(seatIndex, 1);
        cell.classList.remove('selected');
        this.selectedSeatsTotal -= 1;
      }
    }
  }

  reserveSeats() {
    this.seatService.postReserveSeats(this.getParamId,this.selectedSeatsTabToUpdate).subscribe((res) =>{
      console.log("Successfull", res)
      this.router.navigate([''])
    }, (err)=> console.log(err))
  }

  cancelReservation() {
    this.router.navigate(['/film',this.getParamId])
  }

}
