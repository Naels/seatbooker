import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MoviereservationComponent } from './moviereservation.component';

describe('MoviereservationComponent', () => {
  let component: MoviereservationComponent;
  let fixture: ComponentFixture<MoviereservationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MoviereservationComponent]
    });
    fixture = TestBed.createComponent(MoviereservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
