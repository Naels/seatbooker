import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-headerreservation',
  templateUrl: './headerreservation.component.html',
  styleUrls: ['./headerreservation.component.css']
})
export class HeaderreservationComponent implements OnInit {

  @Input() filmID: any;

  headerStyles: any;

  ngOnInit() {
    this.headerStyles = {
      '--film-image': `linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), url('../../../assets/images/films/${this.filmID}.jpg')`
    };
  }

}
