import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderreservationComponent } from './headerreservation.component';

describe('HeaderreservationComponent', () => {
  let component: HeaderreservationComponent;
  let fixture: ComponentFixture<HeaderreservationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderreservationComponent]
    });
    fixture = TestBed.createComponent(HeaderreservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
