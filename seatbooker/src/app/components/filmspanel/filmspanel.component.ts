import { Component, OnInit } from '@angular/core';

import { MovieService } from 'src/app/services/movie/movie.service';
import { ShowtimeService } from 'src/app/services/showtime/showtime.service';


@Component({
  selector: 'app-filmspanel',
  templateUrl: './filmspanel.component.html',
  styleUrls: ['./filmspanel.component.css'],
})
export class FilmspanelComponent implements OnInit {

  movies            : any 
  showtimes         : any
  filteredShowtimes : any

  constructor(private movieService    : MovieService,
              private showtimeService : ShowtimeService){}

  ngOnInit(){
    this.getAllMovies()
    this.getAllShowtimes()

  }

  getAllMovies() {
    this.movieService.getMovies().subscribe((res) =>{
      this.movies = res
    }, (err) => console.log(err))
  }

  getAllShowtimes() {
    this.showtimeService.getShowtimes().subscribe((res) =>{
      this.showtimes = res
    }, (err) => console.log(err))
  }

}
