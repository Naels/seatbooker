import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilmspanelComponent } from './filmspanel.component';

describe('FilmspanelComponent', () => {
  let component: FilmspanelComponent;
  let fixture: ComponentFixture<FilmspanelComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FilmspanelComponent]
    });
    fixture = TestBed.createComponent(FilmspanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
