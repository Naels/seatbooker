import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'showtimeFilter'
})
export class ShowtimeFilterPipe implements PipeTransform {
  transform(showtimes: any[], movieId: number): any[] {
    return showtimes?.filter(showtime => showtime?.movie_id === movieId);
  }
}