import { Component, OnInit } from '@angular/core';
import { MovieService } from 'src/app/services/movie/movie.service';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-filminfos',
  templateUrl: './filminfos.component.html',
  styleUrls: ['./filminfos.component.css']
})
export class FilminfosComponent implements OnInit {

  filmInfos: any
  showtimes: any

  constructor(private movieService: MovieService,
              private router      : ActivatedRoute) { }

  ngOnInit(): void {
    let getParamId = this.router.snapshot.paramMap.get('id')
    this.getFilmById(getParamId)
    this.getShowtimesByMovieId(getParamId)
  }

  getFilmById(id: any) {
    this.movieService.getMovieById(id).subscribe((res) => {
      this.filmInfos = res
    }, (err) => console.log(err))
  }

  getShowtimesByMovieId(id: any) {
    this.movieService.getShowtimesByMovieId(id).subscribe((res) =>{
      this.showtimes = res.showtimes
    }, (err) =>console.log(err))
  }

}
