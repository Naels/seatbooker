import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilminfosComponent } from './filminfos.component';

describe('FilminfosComponent', () => {
  let component: FilminfosComponent;
  let fixture: ComponentFixture<FilminfosComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FilminfosComponent]
    });
    fixture = TestBed.createComponent(FilminfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
