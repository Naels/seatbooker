import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FilmspanelComponent } from './components/filmspanel/filmspanel.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FilminfosComponent } from './components/filminfos/filminfos/filminfos.component';
import { MainComponent } from './components/main/main/main.component';
import { HeaderComponent } from './components/header/header/header.component';
import { MoviereservationComponent } from './components/moviereservation/moviereservation.component';
import { ShowtimeFilterPipe } from './components/filmspanel/showtime-filter.pipe';
import { LoggingInterceptor } from './interceptors/LoggingInterceptor';
import { HeaderreservationComponent } from './components/headerreservation/headerreservation.component';

@NgModule({
  declarations: [
    AppComponent,
    FilmspanelComponent,
    FilminfosComponent,
    MainComponent,
    HeaderComponent,
    MoviereservationComponent, 
    ShowtimeFilterPipe, HeaderreservationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: LoggingInterceptor,
    multi: true
  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
