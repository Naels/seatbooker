import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShowtimeService {
  private apiUrl = 'http://localhost:3000';
  private service = '/showtimes'

  constructor(private http: HttpClient) { }

  getShowtimes(): Observable<any> {
    return this.http.get(`${this.apiUrl}${this.service}`);
  }

  getShowtimesByMovieId(movieId: number): Observable<any> {
    return this.http.get(`${this.apiUrl}${this.service}/${movieId}`);
  }

  getShowtimeId(movieId: number): Observable<any> {
    return this.http.get(`${this.apiUrl}${this.service}/show/${movieId}`);
  }

  // createShowtime(showtimeData: any): Observable<any> {
  //   return this.http.post(`${this.apiUrl}${this.service}`, showtimeData);
  // }

  // updateShowtime(showtimeId: number, showtimeData: any): Observable<any> {
  //   return this.http.put(`${this.apiUrl}${this.service}/${showtimeId}`, showtimeData);
  // }

  // deleteShowtime(showtimeId: number): Observable<any> {
  //   return this.http.delete(`${this.apiUrl}${this.service}/${showtimeId}`);
  // }
}