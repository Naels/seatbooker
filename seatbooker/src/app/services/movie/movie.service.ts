import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  private apiUrl  = 'http://localhost:3000';
  private service = '/movies'

  constructor(private http: HttpClient) { }

  getMovies(): Observable<any> {
    return this.http.get(`${this.apiUrl}${this.service}`);
  }

  getMovieById(movieId: number): Observable<any> {
    return this.http.get(`${this.apiUrl}${this.service}/${movieId}`);
  }

  createMovie(movieData: any): Observable<any> {
    return this.http.post(`${this.apiUrl}${this.service}`, movieData);
  }

  updateMovie(movieId: number, movieData: any): Observable<any> {
    return this.http.put(`${this.apiUrl}${this.service}/${movieId}`, movieData);
  }

  deleteMovie(movieId: number): Observable<any> {
    return this.http.delete(`${this.apiUrl}${this.service}/${movieId}`);
  }

  getShowtimesByMovieId(movieId: number): Observable<any> {
    return this.http.get(`${this.apiUrl}${this.service}/${movieId}/showtimes`)
  }
}