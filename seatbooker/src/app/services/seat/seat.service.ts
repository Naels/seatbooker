import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class SeatService {

  private apiUrl = 'http://localhost:3000'
  private service = '/seats'

  constructor(private http: HttpClient) { }

  getSeatByShowtimeId(showtimeId: number): Observable<any> {
    return this.http.get(`${this.apiUrl}${this.service}/${showtimeId}`)
  }

  getCountAvailableSeatsByShowtime(showtimeId: number): Observable<any> {
    return this.http.get(`${this.apiUrl}${this.service}/${showtimeId}/countAvailable`)
  }

  postReserveSeats(showtimeId: number, seatsIDs: number[]): Observable<any> {
    const url = `${this.apiUrl}${this.service}/${showtimeId}/seats`
    const payload = {seats: seatsIDs}
    return this.http.post(url, payload)
  }
}
