# SeatBooker App

This Angular application allows users to book precise seats in a cinema room for a specific film and showtime. It consumes the Cinema Seat Booking Service API to retrieve seat availability and make bookings.

### Features

1. View available films and showtimes
2. Select desired film and desired showtime
3. See places already booked 
4. Select seats and book them for a specific showtime

### Prerequisites

Before running the application, make sure you have the following installed:

    Node.js (version 14)
    Angular CLI (version 16)

### Installation

Navigate to the project directory : seatbooker
Install the dependencies using `npm i`

### Configuration & usage
Nothing to change about configuration, just use `Dump-seatbooker` from the swbeservice project to build the correct database
Then run `npm start`

Not working on phone resolution yet, use it on PC :)

Enjoy your cinema seat booking experience!
